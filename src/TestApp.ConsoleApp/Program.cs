﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using System.Threading;
using TestApp.Interfaces.Processing;
using TestApp.Processing.Fabric;
using TestApp.Processing.Service;

namespace TestApp.ConsoleApp
{
	class Program
	{
		static void Main(string[] args)
		{
			using (var catalog = new AggregateCatalog())
			{
				catalog.Catalogs.Add(new AssemblyCatalog(typeof(Program).Assembly));
				catalog.Catalogs.Add(new AssemblyCatalog(typeof(ProcessingTaskFactory).Assembly));

				using (var container = new CompositionContainer(catalog))
				{
					Console.WriteLine($"Current thread: {Thread.CurrentThread.ManagedThreadId}");

					var taskFactories = container.GetExportedValues<IProcessTaskFactory>();
					var tasks = new List<IProcessTask>();
					foreach (var taskFactory in taskFactories)
					{
						tasks.AddRange(taskFactory.CreateTasks(message =>
						{
							Console.WriteLine($"{message.ProcessName}: {message.Message}");
						}));
					}

					var processingService = container.GetExportedValue<ProcessingService>();
					var task = processingService.RunAsync(tasks);

					Console.WriteLine("Processes started");
					
					task.Wait();

					Console.WriteLine("Processes completed. Press \"Enter\" to exit");
					Console.ReadKey();

				}
			}

		}

	}
}
