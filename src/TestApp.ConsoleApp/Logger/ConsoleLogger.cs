﻿using System;
using System.ComponentModel.Composition;
using System.Diagnostics;
using TestApp.Interfaces.Logger;

namespace TestApp.ConsoleApp.Logger
{
	[Export(typeof(ILogger))]
	public class Logger : ILogger
	{
		public void WriteMessage(string message, EventLogEntryType type = EventLogEntryType.Information)
		{
			Console.WriteLine(message);
		}
	}
}
