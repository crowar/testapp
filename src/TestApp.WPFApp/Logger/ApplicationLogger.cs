﻿using System.ComponentModel.Composition;
using System.Diagnostics;
using TestApp.Interfaces.Logger;

namespace TestApp.WPFApp.Logger
{
	[Export(typeof(ILogger))]
	public class ApplicationLogger : ILogger
	{
		public void WriteMessage(string message, EventLogEntryType type = EventLogEntryType.Information)
		{
			//Console.WriteLine(message);
		}
	}
}
