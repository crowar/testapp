﻿using System;
using System.ComponentModel.Composition;
using System.Configuration;
using System.IO;
using Newtonsoft.Json;
using TestApp.Configuration;
using TestApp.Configuration.Convertor;
using TestApp.Interfaces.Configuration;

namespace TestApp.WPFApp.Configuration
{
	[Export(typeof(IConfigService))]
	public class ApplicationConfigService : IConfigService
	{
		public Config Config { get; private set; }

		public ApplicationConfigService()
		{
			var settingFile = new FileInfo(ConfigurationManager.AppSettings["testapp.config.path"] ?? @"config\TestApp.Config.Debug.json");
			try
			{
				var settingsJson = File.ReadAllText(settingFile.FullName);
				var setting = JsonConvert.DeserializeObject<dynamic>(settingsJson);
				this.Config = ConfigJsonConverter.Convert(setting);
			}
			catch (Exception ex)
			{
				throw new InvalidOperationException($"Error of TestApp.config.File '{settingFile.FullName}'.", ex);
			}
		}
	}
}
