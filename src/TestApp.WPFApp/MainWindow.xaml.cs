﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using TestApp.Interfaces.Configuration;
using TestApp.Interfaces.Processing;
using TestApp.Library;
using TestApp.Processing.Fabric;
using TestApp.Processing.Service;
using TestApp.WPFApp.Controls;

namespace TestApp.WPFApp
{
	public partial class MainWindow : Window
	{
		private readonly AggregateCatalog _catalog;
		private readonly CompositionContainer _container;
		private readonly List<IProcessTask> _tasks;

		public MainWindow()
		{
			_tasks = new List<IProcessTask>();

			_catalog = new AggregateCatalog();
			_catalog.Catalogs.Add(new AssemblyCatalog(typeof(App).Assembly));
			_catalog.Catalogs.Add(new AssemblyCatalog(typeof(ProcessingTaskFactory).Assembly));
			_container = new CompositionContainer(_catalog);

			InitializeComponent();
		}

		private void BtnLoadConfig_Click(object sender, RoutedEventArgs e)
		{
			BtnLoadConfig.IsEnabled = false;

			new Task(() =>
			{
				foreach (var processConfigSection in _container.GetExportedValue<IConfigService>().Config.ProcessSettings)
				{
					ProcessesPanel.Dispatcher.BeginInvoke(new Action(() =>
						ProcessesPanel.Children.Add(new ProcessControl
						{
							ProcessName = processConfigSection.Name,
							ProcessMessage = "Информатор создан",
							Margin = new Thickness(10, 0, 10, 10),
							Name = processConfigSection.Name
						}))
					);
				}

				var taskFactories = _container.GetExportedValues<IProcessTaskFactory>();
				foreach (var taskFactory in taskFactories)
				{
					_tasks.AddRange(taskFactory.CreateTasks(ChangeProcessStatus));
				}

				Dispatcher.BeginInvoke(new Action(() => BtnRunProcess.IsEnabled = true));
			}).Start();
		}

		private void BtnRunProcess_Click(object sender, RoutedEventArgs e)
		{
			var processingService = _container.GetExportedValue<ProcessingService>();
			var task = processingService.RunAsync(_tasks);
		}

		private void ChangeProcessStatus(ProcessFeedbackMessage processFeedbackMessage)
		{
			Dispatcher.BeginInvoke(new Action(() =>
			{
				var proc = ProcessesPanel.Children.OfType<ProcessControl>().FirstOrDefault(r => r.Name == processFeedbackMessage.ProcessName);

				if (proc == null) return;

				proc.ProcessMessage = processFeedbackMessage.Message;
				proc.Status(processFeedbackMessage.Status);
			}));
		}

		private void Window_Closed(object sender, EventArgs e)
		{
			_container.Dispose();
			_catalog.Dispose();
		}
	}
}
