﻿using System;
using System.Windows.Controls;
using System.Windows.Media;
using TestApp.Library;

namespace TestApp.WPFApp.Controls
{
	/// <summary>
	/// Interaction logic for ProcessControl.xaml
	/// </summary>
	public partial class ProcessControl : UserControl
	{
		public ProcessControl()
		{
			InitializeComponent();
		}

		public string ProcessName {
			get { return LblName.Content.ToString(); }
			set { LblName.Content = value; }
		}
		public string ProcessMessage {
			get { return LblMessage.Content.ToString(); }
			set { LblMessage.Content = value; }
		}

		public void Status(ProcessStatus status)
		{
			var color = Color.FromRgb(163, 163, 163);

			switch (status)
			{
				case ProcessStatus.Created:
					color = Color.FromRgb(153, 213, 255);
					break;
				case ProcessStatus.Working:
					color = Color.FromRgb(245, 89, 13);
					break;
				case ProcessStatus.Complited:
					color = Color.FromRgb(56, 234, 65);
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(status));
			}

			foreach (var gradientStop in GradientBrush.GradientStops)
			{
				color.A = gradientStop.Color.A;
				gradientStop.Color = color;
			}
		}
	}
}
