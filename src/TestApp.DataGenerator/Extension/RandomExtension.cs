﻿using System;

namespace TestApp.DataGenerator.Extension
{
	public static class RandomExtension
	{
		public static DateTime NextDateTime(this Random rnd, int minValue, int maxValue)
		{
			var dt = DateTime.Now
				.AddDays(rnd.Next(minValue, maxValue))
				.AddHours(rnd.Next(minValue, maxValue))
				.AddHours(rnd.Next(minValue, maxValue))
				.AddMinutes(rnd.Next(minValue, maxValue))
				.AddSeconds(rnd.Next(minValue, maxValue))
				.AddMilliseconds(rnd.Next(minValue, maxValue));

			return dt;
		}
	}
}
