﻿using System.Collections.ObjectModel;

namespace TestApp.DataGenerator
{
	public class GenerateTypeObservableCollection : ObservableCollection<GenerateTypeCollectionItem>
	{
		public GenerateTypeObservableCollection()
		{
			Add(new GenerateTypeCollectionItem { Type = GenerateType.GenInt, Description = "Целое число" });
			Add(new GenerateTypeCollectionItem { Type = GenerateType.GenDateTime, Description = "Время" });
		}
	}

	public class GenerateTypeCollectionItem
	{
		public GenerateType Type { get; set; }
		public string Description { get; set; }
		
		public override string ToString()
		{
			return Description;
		}
	}

	public enum GenerateType
	{
		GenInt,
		GenDateTime
	}
}
