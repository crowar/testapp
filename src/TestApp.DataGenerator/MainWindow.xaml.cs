﻿using System;
using System.Globalization;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using TestApp.DataGenerator.Extension;

namespace TestApp.DataGenerator
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();
		}

		private void GenerateData(object sender, RoutedEventArgs e)
		{
			var selectedItem = cbDataType.SelectedValue as GenerateTypeCollectionItem;

			if (selectedItem == null) return;
			if (!IsTextAllowed(tbRows.Text)) return;

			var rows = int.Parse(tbRows.Text);
			var rnd = new Random();

			if (!Directory.Exists("files"))
				Directory.CreateDirectory("files");

			switch (selectedItem.Type)
			{
				case GenerateType.GenInt:
					using (var writetext = new StreamWriter("files\\NumberFile.txt", true, Encoding.UTF8))
					{
						for (var i = 0; i < rows; i++)
						{
							writetext.WriteLine(rnd.Next(0, 100));
						}
					}
					break;
				case GenerateType.GenDateTime:
					using (var writetext = new StreamWriter("files\\DateFile.txt", true, Encoding.UTF8))
					{
						for (var i = 0; i < rows; i++)
						{
							writetext.WriteLine(rnd.NextDateTime(0, 10000).ToString(tbFormat.Text ?? "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture));
						}
					}
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}



		private static bool IsTextAllowed(string text)
		{
			var regex = new Regex("^[0-9]{1,}$");
			return regex.IsMatch(text);
		}
	}

}
