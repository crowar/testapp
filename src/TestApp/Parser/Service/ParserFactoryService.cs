﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using TestApp.Interfaces.Factory;
using TestApp.Interfaces.Parser;

namespace TestApp.Parser.Service
{
	[Export(typeof(IFactoryService<IFileParser>))]
	public class ParserFactoryService : IFactoryService<IFileParser>
	{
		private readonly List<IFactory<IFileParser>> _providers;

		[ImportingConstructor]
		public ParserFactoryService([ImportMany] IEnumerable<IFactory<IFileParser>> parserProviders)
		{
			if (parserProviders == null) throw new ArgumentNullException(nameof(parserProviders));

			var overdefinedParsers = parserProviders
				.GroupBy(p => p.Type)
				.Where(g => g.Count() > 1)
				.Select(g => g.Key);

			if (overdefinedParsers.Any())
				throw new ArgumentException($"Overdefined parsers found : [{string.Join(";", overdefinedParsers.ToArray())}]");

			this._providers = new List<IFactory<IFileParser>>(parserProviders);
		}

		public IFileParser Create(string parserType, dynamic parserParameters)
		{
			if (string.IsNullOrEmpty(parserType)) throw new ArgumentNullException(nameof(parserType));

			var provider = this._providers.FirstOrDefault(p => p.Type.Equals(parserType, StringComparison.OrdinalIgnoreCase));

			if (provider == null)
				throw new InvalidOperationException($"Parser {parserType} is not found.");

			return provider.Create(parserParameters);
		}
	}
}
