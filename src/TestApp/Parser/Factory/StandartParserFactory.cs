﻿using System.ComponentModel.Composition;
using TestApp.Interfaces.Factory;
using TestApp.Interfaces.Parser;

namespace TestApp.Parser.Factory
{
	[Export(typeof(IFactory<IFileParser>))]
	public class StandartParserFactory: IFactory<IFileParser>
	{
		public string Type => typeof (StandartFileParser).FullName;

		public IFileParser Create(dynamic parameters)
		{
			return new StandartFileParser();
		}
	}
}
