﻿using TestApp.Interfaces.Parser;
using TestApp.Interfaces.Reader;
using TestApp.Library;

namespace TestApp.Parser
{
	public class StandartFileParser : IFileParser
	{
		public bool TryToParse(IFileReader fileReader, out StandartFileRecord record)
		{
			var line = fileReader.ReadNextLine();

			//Can be performed complex analysis of the line if needed

			record = new StandartFileRecord
			{
				Data = line
			};

			return true;
		}
	}
}
