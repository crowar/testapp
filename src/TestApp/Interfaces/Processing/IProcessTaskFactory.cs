﻿using System;
using System.Collections.Generic;
using TestApp.Library;

namespace TestApp.Interfaces.Processing
{
	public interface IProcessTaskFactory
	{
		IEnumerable<IProcessTask> CreateTasks(Action<ProcessFeedbackMessage> action);
	}
}