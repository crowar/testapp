﻿namespace TestApp.Interfaces.Processing
{
	public interface IProcessTask
	{
		bool Do();
	}
}
