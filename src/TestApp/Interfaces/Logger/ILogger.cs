﻿using System.Diagnostics;

namespace TestApp.Interfaces.Logger
{
	public interface ILogger
	{
		void WriteMessage(string message, EventLogEntryType type = EventLogEntryType.Information);
	}
}
