﻿using TestApp.Interfaces.Reader;
using TestApp.Library;

namespace TestApp.Interfaces.Parser
{
	public interface IFileParser
	{
		bool TryToParse(IFileReader fileReader, out StandartFileRecord record);
	}
}
