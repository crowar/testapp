﻿using TestApp.Library;

namespace TestApp.Interfaces.Transformer
{
	public interface ITransformer
	{
		bool TryToTransform(StandartFileRecord inputRecord, out StandartFileRecord outRecord);
	}
}
