﻿using System;

namespace TestApp.Interfaces.Reader
{
	public interface IFileReader : IDisposable
	{
		bool EndOfLog { get; }
		string ReadNextLine();

		string CurrentLine { get; }
		long CurrentLineNumber { get; }
	}
}
