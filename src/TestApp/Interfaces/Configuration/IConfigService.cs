﻿using TestApp.Configuration;

namespace TestApp.Interfaces.Configuration
{
	public interface IConfigService
	{
		Config Config { get; }
	}
}
