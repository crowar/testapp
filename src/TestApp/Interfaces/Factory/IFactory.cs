﻿namespace TestApp.Interfaces.Factory
{
	public interface IFactory<T>
	{
		string Type { get; }
		T Create(dynamic parameters);
	}
}
