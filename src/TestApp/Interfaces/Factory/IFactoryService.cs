﻿namespace TestApp.Interfaces.Factory
{
	public interface IFactoryService<T>
	{
		T Create(string type, dynamic parameters);
	}
}
