﻿using System;
using System.Collections.Generic;
using System.Linq;
using TestApp.Configuration.ConfigSection;

namespace TestApp.Configuration.Convertor
{
	public static class ConfigJsonConverter
	{
		public static Config Convert(dynamic setting)
		{
			if (setting == null) throw new ArgumentNullException(nameof(setting));

			return Config.Create(ParseProcesses(setting));
		}

		private static IEnumerable<ProcessConfigSection> ParseProcesses(dynamic setting)
		{
			var sources = setting.Processes as IEnumerable<dynamic>;

			if (sources == null) throw new InvalidOperationException("Sources not found.");

			return sources.Select(proc => ProcessConfigConvert(proc)).Cast<ProcessConfigSection>();
		}

		private static ProcessConfigSection ProcessConfigConvert(dynamic setting)
		{
			if (setting == null) throw new ArgumentNullException(nameof(setting));

			return ProcessConfigSection.Create(
				setting.Name.ToObject<string>(),
				ParserConfigConvert(setting.Parser),
				TransformerConfigConvert(setting.Transformer),
				OptionsConfigConvert(setting.Options)
				);
		}

		private static ParserConfigSection ParserConfigConvert(dynamic parser)
		{
			if (parser == null) throw new ArgumentNullException(nameof(parser));

			return ParserConfigSection.Create(
				parser.Type.ToObject<string>(),
				parser.Count.ToObject<int>()
			);
		}

		private static TransformerConfigSection TransformerConfigConvert(dynamic transformer)
		{
			if (transformer == null) throw new ArgumentNullException(nameof(transformer));
			
			return TransformerConfigSection.Create(
				transformer.Type.ToObject<string>(), 
				transformer.Parameters);
		}

		private static OptionsConfigSection OptionsConfigConvert(dynamic options)
		{
			if (options == null) throw new ArgumentNullException(nameof(options));

			return OptionsConfigSection.Create(
				options.SourceFile.ToObject<string>(),
				options.DestinationFile.ToObject<string>(),
				options.DestinationWithOriginalValue.ToObject<bool>());
		}

	}
}
