﻿using System;
using System.Collections.Generic;
using TestApp.Configuration.ConfigSection;

namespace TestApp.Configuration
{
	public class Config
	{
		public IEnumerable<ProcessConfigSection>  ProcessSettings { get; private set; }

		internal static Config Create(IEnumerable<ProcessConfigSection> processSettings)
		{
			if (processSettings == null)
				throw new ArgumentNullException(nameof(processSettings));

			return new Config
			{
				ProcessSettings = new List<ProcessConfigSection>(processSettings)
			};
		}
	}
}
