﻿using System;

namespace TestApp.Configuration.ConfigSection
{
	public class ProcessConfigSection
	{
		public string Name { get; set; }
		public ParserConfigSection Parser { get; set; }
		public TransformerConfigSection Transformer { get; set; }
		public OptionsConfigSection Options { get; set; }

		internal static ProcessConfigSection Create(string name, ParserConfigSection parser, TransformerConfigSection transformer,
			OptionsConfigSection options)
		{
			if (string.IsNullOrEmpty(name)) throw new ArgumentException("Argument is null or empty", nameof(name));
			if (parser == null) throw new ArgumentNullException(nameof(parser));
			if (transformer == null) throw new ArgumentNullException(nameof(transformer));
			if (options == null) throw new ArgumentNullException(nameof(options));

			return new ProcessConfigSection
			{
				Name = name,
				Parser = parser,
				Transformer = transformer,
				Options = options
			};
		}
	}
}
