﻿using System;

namespace TestApp.Configuration.ConfigSection
{
	public class ParserConfigSection
	{
		public string Type { get; set; }
		public int ThreadCount { get; set; }
		
		internal static ParserConfigSection Create(string type, int threadCount)
		{
			if (string.IsNullOrEmpty(type)) throw new ArgumentException("Argument is null or empty", nameof(type));
			if (threadCount <= 0) throw new ArgumentException("Argument can not be less or equal to zero", nameof(threadCount));

			return new ParserConfigSection
			{
				Type = type,
				ThreadCount = threadCount
			};
		}
	}
}
