﻿using System;

namespace TestApp.Configuration.ConfigSection
{
	public class TransformerConfigSection
	{
		public string Type { get; set; }
		public dynamic Parameters { get; set; }

		internal static TransformerConfigSection Create(string type, dynamic parameters)
		{
			if (string.IsNullOrEmpty(type)) throw new ArgumentException("Argument is null or empty", nameof(type));

			return new TransformerConfigSection
			{
				Type = type,
				Parameters = parameters
			};
		}
	}
}
