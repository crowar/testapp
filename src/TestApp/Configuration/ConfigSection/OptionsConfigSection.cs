﻿using System;

namespace TestApp.Configuration.ConfigSection
{
	public class OptionsConfigSection
	{
		public string SourceFile { get; set ; }
		public string DestinationFile { get; set ; }
		public bool DestinationWithOriginalValue { get; set; }

		internal static OptionsConfigSection Create(string sourceFile, string destinationFile, bool destinationWithOriginalValue)
		{
			if (string.IsNullOrEmpty(sourceFile)) throw new ArgumentException("Argument is null or empty", nameof(sourceFile));
			if (string.IsNullOrEmpty(destinationFile)) throw new ArgumentException("Argument is null or empty", nameof(destinationFile));

			return new OptionsConfigSection
			{
				SourceFile = sourceFile,
				DestinationFile = destinationFile,
				DestinationWithOriginalValue = destinationWithOriginalValue
			};
		}
	}
}
