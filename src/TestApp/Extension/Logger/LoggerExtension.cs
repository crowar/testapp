﻿using TestApp.Interfaces.Logger;

namespace TestApp.Extension.Logger
{
	public static class LoggerExtension
	{
		public static void WriteMessage(this ILogger logger, string format, params object[] args)
		{
			logger.WriteMessage(string.Format(format, args));
		}
	}
}
