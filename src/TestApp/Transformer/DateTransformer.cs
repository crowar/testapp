﻿using System;
using System.Globalization;
using TestApp.Interfaces.Transformer;
using TestApp.Library;

namespace TestApp.Transformer
{
	public class DateTransformer : ITransformer
	{
		private readonly string _sourceDateFormat;
		private readonly string _destinationDateFormat;

		private DateTransformer(string sourceDateFormat, string destinationDateFormat)
		{
			this._sourceDateFormat = sourceDateFormat;
			this._destinationDateFormat = destinationDateFormat;
		}

		public bool TryToTransform(StandartFileRecord inputRecord, out StandartFileRecord outRecord)
		{
			outRecord = default(StandartFileRecord);

			DateTime dateTime;
			if (!DateTime.TryParseExact(inputRecord.Data, _sourceDateFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTime))
				return false;

			outRecord = new StandartFileRecord
			{
				Data = dateTime.ToString(_destinationDateFormat)
			};

			return true;
		}

		public static ITransformer Create(dynamic parameters)
		{
			return new DateTransformer(
				parameters.SourceDateFormat.ToObject<string>(),
				parameters.DestinationDateFormat.ToObject<string>());
		}
	}
}
