﻿using System.ComponentModel.Composition;
using TestApp.Interfaces.Factory;
using TestApp.Interfaces.Transformer;

namespace TestApp.Transformer.Factory
{
	[Export(typeof(IFactory<ITransformer>))]
	public class DateTransformerFactory : IFactory<ITransformer>
	{
		public string Type => typeof(DateTransformer).FullName;
		public ITransformer Create(dynamic parameters)
		{
			return DateTransformer.Create(parameters);
		}
	}
}
