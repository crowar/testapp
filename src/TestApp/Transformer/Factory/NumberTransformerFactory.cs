﻿using System.ComponentModel.Composition;
using TestApp.Interfaces.Factory;
using TestApp.Interfaces.Transformer;

namespace TestApp.Transformer.Factory
{
	[Export(typeof(IFactory<ITransformer>))]
	public class NumberTransformerFactory : IFactory<ITransformer>
	{
		public string Type => typeof (NumberTransformer).FullName;

		public ITransformer Create(dynamic parameters)
		{
			return NumberTransformer.Create(parameters);
		}
	}
}
