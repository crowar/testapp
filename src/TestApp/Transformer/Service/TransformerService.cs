﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using TestApp.Interfaces.Factory;
using TestApp.Interfaces.Transformer;

namespace TestApp.Transformer.Service
{
	[Export(typeof(IFactoryService<ITransformer>))]
	public class TransformerService : IFactoryService<ITransformer>
	{
		private readonly List<IFactory<ITransformer>> _providers;

		[ImportingConstructor]
		public TransformerService([ImportMany] IEnumerable<IFactory<ITransformer>> transformerProviders)
		{
			if (transformerProviders == null) throw new ArgumentNullException(nameof(transformerProviders));

			var overdefinedTransformers = transformerProviders
				.GroupBy(p => p.Type)
				.Where(g => g.Count() > 1)
				.Select(g => g.Key);

			if (overdefinedTransformers.Any())
				throw new ArgumentException($"Overdefined transformer found : [{string.Join(";", overdefinedTransformers.ToArray())}]");

			this._providers = new List<IFactory<ITransformer>>(transformerProviders);
		}

		public ITransformer Create(string transformerType, dynamic transformerParameters)
		{
			if (string.IsNullOrEmpty(transformerType)) throw new ArgumentNullException(nameof(transformerType));

			var provider = this._providers.FirstOrDefault(p => p.Type.Equals(transformerType, StringComparison.OrdinalIgnoreCase));

			if (provider == null)
				throw new InvalidOperationException($"Transformer {transformerType} is not found.");

			return provider.Create(transformerParameters);
		}
	}
}
