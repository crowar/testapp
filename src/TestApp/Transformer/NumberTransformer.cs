﻿using System.Numerics;
using TestApp.Interfaces.Transformer;
using TestApp.Library;

namespace TestApp.Transformer
{
	public class NumberTransformer : ITransformer
	{
		private static BigInteger Factorial(uint number)
		{
			return (number == 0) ? 1 : BigInteger.Multiply(Factorial(number - 1), new BigInteger(number));
		}

		public bool TryToTransform(StandartFileRecord inputRecord, out StandartFileRecord outRecord)
		{
			outRecord = default(StandartFileRecord);

			uint number;
			if (!uint.TryParse(inputRecord.Data, out number))
				return false;

			outRecord = new StandartFileRecord
			{
				Data = Factorial(number).ToString()
			};

			return true;
		}

		public static ITransformer Create(dynamic parameters)
		{
			return new NumberTransformer();
		}
	}
}
