﻿using System;
using System.IO;
using TestApp.Interfaces.Reader;

namespace TestApp.Reader
{
	public class FileReader : IFileReader
	{
		private readonly StreamReader _reader;

		public FileReader(FileInfo fileInfo)
		{
			try
			{
				_reader = new StreamReader(fileInfo.FullName);
			}
			catch (Exception)
			{
				Dispose();
				throw;
			}
		}

		public string ReadNextLine()
		{
			CurrentLineNumber++;
			CurrentLine = this._reader.ReadLine();
			return CurrentLine;
		}

		public bool EndOfLog => _reader.EndOfStream;

		public string CurrentLine { get; private set; }
		public long CurrentLineNumber { get; private set; }

		public void Dispose()
		{
			if (!object.ReferenceEquals(this._reader, null))
				this._reader.Dispose();
		}

	}
}
