﻿namespace TestApp.Library
{
	public class ProcessFeedbackMessage
	{
		public string ProcessName { get; set; }
		public ProcessStatus Status { get; set; }
		public string Message { get; set; }
		public int ThreadId { get; set; }
	}

	public enum ProcessStatus
	{
		Created,
		Working,
		Complited
	}
}
