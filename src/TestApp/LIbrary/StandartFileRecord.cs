﻿namespace TestApp.Library
{
	public class StandartFileRecord
	{
		public string Data { get; set; }

		public static StandartFileRecord Create(string data)
		{
			return new StandartFileRecord
			{
				Data = data
			};
		}
	}
}
