﻿using System;
using System.IO;
using System.Text;
using System.Threading;
using TestApp.Configuration.ConfigSection;
using TestApp.Interfaces.Logger;
using TestApp.Interfaces.Parser;
using TestApp.Interfaces.Processing;
using TestApp.Interfaces.Transformer;
using TestApp.Library;
using TestApp.Reader;

namespace TestApp.Processing
{
	public class ProcessingTask : IProcessTask
	{
		private readonly ILogger _logger;
		private readonly ProcessConfigSection _processSetting;
		private readonly IFileParser _parser;
		private readonly ITransformer _transformer;
		private readonly Action<ProcessFeedbackMessage> _action;

		public ProcessingTask(ILogger logger, ProcessConfigSection processSetting, IFileParser parser, ITransformer transformer, Action<ProcessFeedbackMessage> action)
		{
			this._logger = logger;
			this._processSetting = processSetting;
			this._parser = parser;
			this._transformer = transformer;
			this._action = action;

			_action(new ProcessFeedbackMessage
			{
				ProcessName = _processSetting.Name,
				Status = ProcessStatus.Created,
				Message = "Процесс создан"
			});
		}

		public bool Do()
		{
			//TODO: Реализовать чтение файла в парсинг файла в несколько потоков

			_action(new ProcessFeedbackMessage
			{
				ProcessName = _processSetting.Name,
				Status = ProcessStatus.Working,
				Message = "Запуск процесса",
				ThreadId = Thread.CurrentThread.ManagedThreadId
			});

			using (var reader = new FileReader(new FileInfo(this._processSetting.Options.SourceFile)))
			{
				var outFilePath = this._processSetting.Options.DestinationFile.
					Replace("{datetime}", DateTime.Now.ToString("yyyyMMddHHmmss")).
					Replace("{threadId}", Thread.CurrentThread.ManagedThreadId.ToString());

				using (var writetext = new StreamWriter(outFilePath, true, Encoding.UTF8))
				{
					_action(new ProcessFeedbackMessage { ProcessName = _processSetting.Name, Message = "Выполняется работа" });

					while (!reader.EndOfLog)
					{
						StandartFileRecord record;

						if (this._parser.TryToParse(reader, out record))
						{
							if (this._processSetting.Options.DestinationWithOriginalValue)
								writetext.Write($"{record.Data} -> ");

							if (this._transformer.TryToTransform(record, out record))
							{
								writetext.WriteLine(record.Data);
							}
						}
					}
				}

				_action(new ProcessFeedbackMessage
				{
					ProcessName = _processSetting.Name,
					Status = ProcessStatus.Complited,
					Message = $"Процесс завершен. Обработано {reader.CurrentLineNumber} строк. Результат сохранен в файл {outFilePath}",
					ThreadId = Thread.CurrentThread.ManagedThreadId
				});
			}

			return true;
		}
	}
}
