﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Threading;
using System.Threading.Tasks;
using TestApp.Extension.Logger;
using TestApp.Interfaces.Configuration;
using TestApp.Interfaces.Logger;
using TestApp.Interfaces.Processing;

namespace TestApp.Processing.Service
{
	[Export]
	public class ProcessingService
	{
		private readonly ILogger _logger;
		private IConfigService _configService;

		[ImportingConstructor]
		public ProcessingService(ILogger logger, IConfigService configService)
		{
			this._logger = logger;
			this._configService = configService;
		}

		public async Task RunAsync(IEnumerable<IProcessTask> tasks)
		{
			this._logger.WriteMessage("Started : {0} - {1}. ", DateTime.Now, Thread.CurrentThread.ManagedThreadId);

			await Task.Factory.StartNew(() =>
			{
				foreach (var task in tasks)
				{
					Task.Factory.StartNew(() => task.Do(), TaskCreationOptions.AttachedToParent);
				}
			});

			this._logger.WriteMessage("Stopped : {0} - {1}. ", DateTime.Now, Thread.CurrentThread.ManagedThreadId);
		}
	}
}
