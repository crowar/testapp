﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using TestApp.Interfaces.Configuration;
using TestApp.Interfaces.Factory;
using TestApp.Interfaces.Logger;
using TestApp.Interfaces.Parser;
using TestApp.Interfaces.Processing;
using TestApp.Interfaces.Transformer;
using TestApp.Library;

namespace TestApp.Processing.Fabric
{
	[Export(typeof(IProcessTaskFactory))]
	public class ProcessingTaskFactory : IProcessTaskFactory
	{
		private readonly ILogger _logger;
		private readonly IFactoryService<IFileParser> _fileParcerFactory;
		private readonly IFactoryService<ITransformer> _transformerFactory;
		private readonly IConfigService _configService;

		[ImportingConstructor]
		public ProcessingTaskFactory(ILogger logger,
									 IFactoryService<IFileParser> fileParcerFactory,
									 IFactoryService<ITransformer> transformerFactory,
									 IConfigService configService)
		{
			this._logger = logger;
			this._fileParcerFactory = fileParcerFactory;
			this._transformerFactory = transformerFactory;
			this._configService = configService;
		}

		public IEnumerable<IProcessTask> CreateTasks(Action<ProcessFeedbackMessage> action)
		{
			var tasks = new List<IProcessTask>();
			foreach (var item in this._configService.Config.ProcessSettings)
			{
				var parser = this._fileParcerFactory.Create(item.Parser.Type, null);
				var transformer = this._transformerFactory.Create(item.Transformer.Type, item.Transformer.Parameters);

				var task = new ProcessingTask(_logger, item, parser, transformer, action);
				tasks.Add(task);
			}
			return tasks;
		}
	}
}
