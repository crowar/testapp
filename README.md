# C#, .Net

**Целевые критерии**: работа с многопоточностью, работа с файлами, шаблоны проектирования, знание английского на базовом уровне.

## 1. Реализовать простейшее приложение.

При запуске .exe файла открывается страница (WPF) с кнопкой «START». При нажатии кнопки

стартуют параллельно 2 потока. В каждом потоке отдельно выполняются задачи 2 и 3,

соответственно. Результаты работы каждого потока должны отображаться в режиме реального

времени в своей области экрана. Примерно экран должен выглядеть так:


```
#!text
+-------------------------------------------------------+
|                         Start                         |
+-------------------------------------------------------+
|          Задача 2         |          Задача 3         |
+---------------------------+---------------------------+
| результат работы потока 1 | результат работы потока 2 |
+---------------------------+---------------------------+

```

В менеджере, который стартует потоки, инициируются настройки.

Оцениваться будут примененные шаблоны проектирования и обработка ошибок.

## 2. Задача 2

Поток получает настройки (настройки инициируются в п.1.):

* имя файла источника.
* имя целевого файла.

**Task**: You are given an integer N. Save into a file the factorial of this number N. In a source file several Ns are given.

**Input**
Input consists of a single integer N , where 1<=N<=100.

**Output**
Save into a file the factorial of N.

**Example**
For an input of 25, you would get 15511210043330985984000000.

## 3. Задача 3.

Поток получает настройки (настройки инициируются в п.1.):

* имя файла источника.
* имя целевого файла.

**Task**: Given a time in 12-hour AM/PM format, convert it to military (24-hour) time. In a source file several dates to convert are given.

**Note**:

Midnight is 12:00:00AM on a 12-hour clock, and 00:00:00 on a 24-hour clock.

Noon is 12:00:00PM on a 12-hour clock, and 12:00:00 on a 24-hour clock.

**Input Format**

A single string containing a time in 12-hour clock format (i.e.: hh:mm:ssAM or hh:mm:ssPM), where

01<=hh<=12 and 01<=mm,ss<=59.

**Output Format**

Convert and save into a file the given time in 24-hour format, where 00<=hh<=23.

**Sample Input**

07:05:45PM

**Sample Output**

19:05:45